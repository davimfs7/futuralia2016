package sql.vigion.com.futuralia2016;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by david on 17-02-2016.
 */
public class SBVactivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sbv);

        //Controlo so swipe
        ViewPager viewPager = (ViewPager) findViewById(R.id.activitymainfragments);

        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager.setAdapter(new MeuFragmentAdapter(fragmentManager));
        viewPager.setCurrentItem(1);
    }

    public void OnClick(View v){
        Intent intent;
        switch ( v.getId() )
        {
            case R.id.buttonSBV:
                intent = new Intent(v.getContext(), InicioSBV.class);
                break;
            case R.id.buttonSobreNos:
                intent = new Intent(v.getContext(), SobreNos.class);
                break;
            //Para botões de Ligar ao 112 correspondentes ao seu ID
            case R.id.button3Ligar1122:
            case R.id.buttonLigar112:
            case R.id.buttonLigar1123:
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:112"));
                break;
            default:
                intent = new Intent(v.getContext(), InfoListView.class);

        }

        startActivity(intent);
    }

}
