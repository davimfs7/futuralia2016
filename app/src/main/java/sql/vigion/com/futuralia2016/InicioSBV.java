package sql.vigion.com.futuralia2016;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class InicioSBV extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sbv);
        //Toast.makeText(getApplicationContext(), "teste", Toast.LENGTH_LONG).show();
    }

    public void OnClick(View v){
        Intent intent;
        intent = new Intent(v.getContext(), SBVFase1.class);
        startActivity(intent);
    }
}
