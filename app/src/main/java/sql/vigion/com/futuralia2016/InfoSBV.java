package sql.vigion.com.futuralia2016;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;


public class InfoSBV extends FragmentActivity implements OnTouchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_sbv);

        findViewById(R.id.imageViewSwipe).bringToFront();


        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearsbv);
        linearLayout.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // ... Respond to touch events
                if( MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_MOVE)
                {
                    findViewById(R.id.imageViewSwipe).setAlpha(0);
                    findViewById(R.id.imageViewSwipe).setVisibility(View.GONE);
                }
                return true;
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) { //?????
        return false;
    }
}
