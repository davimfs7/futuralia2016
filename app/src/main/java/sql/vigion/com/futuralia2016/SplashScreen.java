package sql.vigion.com.futuralia2016;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends Activity {

    Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        activity = this;

        final ImageView img = (ImageView)findViewById(R.id.imageViewSplash);
        final Animation animRotate = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
        final Animation animFadeOut = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);
        img.startAnimation(animRotate);

        animRotate.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //img.startAnimation(animFadeOut);//inicia a animação fade out
                //finish();                      //Termina esta Activity e chama outra

                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                activity.finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
