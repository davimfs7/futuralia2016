package sql.vigion.com.futuralia2016;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;


public class SBVFim extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sbvfim);
    }
    public void OnClick(View v){
        Intent intent;
        switch ( v.getId() )
        {
            case R.id.buttonVoltar:
                intent = new Intent(v.getContext(), MainActivity.class);
                break;
            default:
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:112"));
        }

        startActivity(intent);
    }
}
