package sql.vigion.com.futuralia2016;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;


public class SBVFase3_1 extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sbvfase3_1);
    }

    public void OnClick(View v){
        Intent intent;
        intent = new Intent(v.getContext(), SBVFase3.class);
        startActivity(intent);
    }
}
